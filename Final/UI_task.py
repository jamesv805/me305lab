"""
@file UI_task.py
@author Jeremy Baechler
@author James Verheyden
@date 2021.12.09
@brief UI_task.py is the file which handles all the user interfacing necessary on
the final project.
@details The UI task handles all the communication between the user and the microcontroller.
Any of the six states that the user can choose to move into can be done in this task.
Once the method has transitioned into the new state, it continues to run in that state
until it is reset back to state 1 where it will wait for the user input again.
"""

import pyb
import utime
import touchpanel_driver
import IMU_driver
from ulab import numpy as np
import datacollection
import ustruct
import IMU_task
import motor_driver
import closedloop


class UI_Task:
    '''
    @brief Sets up UI interface task
    @details This UI_Task class is created in order to move through various states
    based in the user's keyboard input. This then transitions the task through various states
    depending on their selection. This task is run in main.py and handles all
    collection and printing of data from the datacollection task.
    '''
    def __init__(self, period, IMU, TP, datacollection, IMU_task, closedloop, motor_1, motor_2):
        '''
        @brief Constructor method for UI_Task class.
        @param period
        period at which the task runs
        @param IMU
        BNO055 class object for accessing IMU data
        @param TP
        TouchPanel object for accessing touch panel data
        @param datacollection
        datacollection task for collecting statespace data
        @param IMU_task
        IMU_task object used for calibration of IMU
        @param closedloop
        closedloop object used to perform control calculations
        @param motor_1
        motor object to drive the first motor
        @param motor_2
        motor object to drive the second motor
        '''
        ## Period at which the task will run
        self.period = period
        ## The next time the loop will run
        self.next_time = utime.ticks_add(utime.ticks_us(), self.period)
        ## State definition, changes based on which key is pressed
        self.STATE = 0
        ## Pyb USB_VCP object for accessing key clicks
        self.serport = pyb.USB_VCP()
        ## Array of ones to pre-allocate the gain vector used in closedloop
        self.K = np.ones([1,4])
        ## Placeholder for gain values entered by user
        self.num_data = ''
        ## Iterator used in State 4 to move through K array
        self.idx = 0
        ## IMU class object
        self.IMU = IMU
        ## Touch panel class object
        self.TP = TP
        ## Data collection class object
        self.DataCollect = datacollection
        ## IMU_task object
        self.IMU_task = IMU_task
        ## Closedloop calculation object
        self.closedloop = closedloop
        ## Motor 1 object
        self.motor1 = motor_1
        ## Motor 2 object
        self.motor2 = motor_2
        
    
    def run(self):
        '''
        @brief Run method which runs the UI_task
        @details This method moves through six different states. State 0 prints the instructions
        for the user interface. State 1 waits for user input. State 2 runs the IMU
        calibration. State 3 runs the touch panel calibration. State 4 runs the K
        selection state. State 5 runs the data collection state. State 6 runs the balance
        ball state.
        '''
        current_time = utime.ticks_us()
        if utime.ticks_diff(current_time, self.next_time) >= 0:
            if self.STATE == 0:
                print('''Press i or I to check IMU calibration
Press p or P to check touch panel calibration
Press k or K to input new controller K values
Press b or B to begin balancing ball
Press s or S to begin data collection
                      ''')
#                print('moving to state 1')
                self.STATE = 1
                
            if self.STATE == 1:
                
                if self.serport.any():
                    val = self.serport.read(1).decode()
                    if val == 'i' or val == 'I':
                        self.STATE = 2
                    if val == 'p' or val == 'P':
                        self.STATE = 3
                        print('going to state 3')
                    if val == 'k' or val == 'K':
                        self.STATE = 4
                        print('Enter K values for controller gain. Hit return after each input value')
                    if val == 's' or val == 'S':
                        self.STATE = 5
                        print('Begin Statespace Data Collection!')
                    if val == 'b' or val == 'B':
                        self.STATE = 6
                        
            if self.STATE == 2:
                self.IMU_task.calibration()
                self.STATE = 1
                
            if self.STATE == 3:
                self.TP.calibration()
                self.STATE = 1
                
            if self.STATE == 4:
#                print(self.K)
                if self.serport.any():
#                    print(self.K)
                    val = self.serport.read(1)
                    if val.isdigit():
                        self.num_data += val.decode()
                        self.serport.write(val.decode())
                    elif val.decode() == '-' and len(self.num_data) == 0:
                        self.num_data += val.decode()
                        self.serport.write(val.decode())
                    elif val.decode() == '.':
                        self.num_data += val.decode()
                        self.serport.write(val.decode())
                    elif val.decode() == '\x7f' and len(self.num_data) > 0:
                        self.num_data = self.num_data[:-1]
                        self.serport.write(val.decode())
                    elif val.decode() == '\r' or val.decode() == '\n':
                        self.serport.write('\r\n')
                        self.K[0][self.idx] = float(self.num_data)
#                        Input K vector for controller gain calcs.
                        print(self.idx)
                        self.num_data = ''
                        self.idx += 1
#                            print('Duty Cycle is: ', self.duty1)
                        
                        if self.idx > 3:
                            print('going to state 1')
                            self.STATE = 1
                            self.num_data = ''
                            
            if self.STATE == 5:
#                StateSpace Data Collection
                data = self.DataCollect.collect()
                storedX1 = [data[0][0], data[0][1], data[0][2], data[0][3]]
                storedX2 = [data[1][0], data[1][1], data[1][2], data[1][3]]
                print(storedX1)
                if self.serport.any():
                    val = self.serport.read(1).decode()
                    if val == 'z' or val == 'Z':
                        self.STATE = 1
                
            if self.STATE == 6:
                while True:
                    try:
                        X = self.DataCollect.collect()
                        duty = self.closedloop.run(self.K, X)
                        print(duty)
                        duty1 = -float(duty[0][0])
                        duty2 = float(-duty[1][0])
                        print(duty1, duty2)
                        self.motor1.set_duty(duty1)
                        self.motor2.set_duty(duty2)
                    except:
                        break
                self.STATE = 1
                pass
                
            self.next_time = utime.ticks_add(self.next_time, self.period) 
        
if __name__ == '__main__':
    i2c = pyb.I2C(1, pyb.I2C.MASTER)
    i2c.init(pyb.I2C.MASTER)
    sensor = IMU_driver.BNO055(i2c, ustruct)
    sensor.op_mode(0x0C)
    pin_ym = pyb.Pin (pyb.Pin.cpu.A0, pyb.Pin.ANALOG)
    pin_xm = pyb.Pin (pyb.Pin.cpu.A1, pyb.Pin.OUT_PP)
    pin_yp = pyb.Pin (pyb.Pin.cpu.A6, pyb.Pin.IN)
    pin_xp = pyb.Pin (pyb.Pin.cpu.A7, pyb.Pin.OUT_PP)
    TP = touchpanel_driver.TouchPanel(pin_ym, pin_xm, pin_yp, pin_xp, 176,100,0)
    datacollection = datacollection.DataCollection(sensor, TP, utime, np)
    IMU_task = IMU_task.IMU_Task('IMU_cal_coeffs')
    closedloop = closedloop.ClosedLoop(utime, 10_00)
    in1 = pyb.Pin(pyb.Pin.cpu.B4, pyb.Pin.OUT_PP)
    in2 = pyb.Pin(pyb.Pin.cpu.B5, pyb.Pin.OUT_PP)
    in3 = pyb.Pin(pyb.Pin.cpu.B0, pyb.Pin.OUT_PP)
    in4 = pyb.Pin(pyb.Pin.cpu.B1, pyb.Pin.OUT_PP)
    tim3 = pyb.Timer(3, freq = 20000)
    t3ch1 = tim3.channel(1, pyb.Timer.PWM, pin=in1)
    t3ch2 = tim3.channel(2, pyb.Timer.PWM, pin=in2)
    t3ch3 = tim3.channel(3, pyb.Timer.PWM, pin=in3)
    t3ch4 = tim3.channel(4, pyb.Timer.PWM, pin=in4)
    motor_drv = motor_driver.DRV8847()
    motor_1 = motor_drv.motor(t3ch1, t3ch2)
    motor_2 = motor_drv.motor(t3ch3, t3ch4)
    
    UI_Task = UI_Task(10_000, sensor, TP, datacollection, IMU_task, closedloop, motor_1, motor_2)
    
    
    
    
    while True:
        UI_Task.run()