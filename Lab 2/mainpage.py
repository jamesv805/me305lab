# -*- coding: utf-8 -*-
"""
@file mainpage.py
@date 10/21/2021
@author Jeremy Baechler

@brief Landing page for Lab Portfolio
@details This page is used to create descriptions for the different labs and include deliverables in ME 305.

@mainpage

@section sec_lab1  Introduction

                   Lab 1: LED Driver
                   Lab 2: Motor Driver and Data Collection UI
                    
@section          \image html FSM_Lab_2.jpg

"""


