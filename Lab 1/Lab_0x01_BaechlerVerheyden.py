'''
@file Lab_0x01_BaechlerVerheyden.py
@brief Controls a green LED on a Nucleo and allows cycling between square, sine, and sawtooth LED patterns
@details Button B1 is used to cycle between square, sine, and sawtooth LED flashes.
            Each patten is displayed through varying the brightness of each LED.
            Apon startup, the program promts the user to press b1 to cycle.
            A keyboard interupt wil stop the program.
            Sourcecode can be found here: https://bitbucket.org/JeremyBaechler/jeremybaechler_me_305/src/master/Lab%201/Lab_0x01_BaechlerVerheyden.py
@date 2021.10.7
@author Jeremy Baechler and James Verheyden

'''


import pyb
import math
import utime

def onButtonPressFCN(IRQ_src):
#    print('Button has been pressed')
    '''
    @brief Determins if button B1 is pressed
    @details Button Flag is a global variable which indicates the down push on b1
    @returns True when the button transisitions from unpressed to pressed
    '''
    global ButtonFlag 
    ButtonFlag = True
    
    
def update_sqw(current_time):
    '''
    @brief Creates square wave
    @details Creates a square wave, starting a new cycle when the button is pressed.
    @returns A value of 100 or 0 for the LED
    '''
    return 100*(current_time/1000 % 1 < 0.5)
    
def update_stw(current_time):
    '''
    @brief Creates sawtooth wave
    @details Creates a sawtooth wave, starting a new cycle when the button is pressed.
    @returns A value between 0 and 100 for the LED
    '''
    return 100*(current_time/1000 % 1.0)
    
def update_sin(current_time):
    '''
    @brief Creates sine wave
    @details Creates a sine wave, starting a new cycle when the button is pressed.
            Returns a value between 50 and 100 so the LED stays constantly lit
            Having it instead return a value between 0 and 100 would cause large gaps where the LED does not emit light due to hardware limitations on low lighting
    @returns A value between 50 and 100 the LED
    '''
    return 50 * math.sin(current_time/(1000*1.6)) + 50
    

if __name__ == '__main__':
    ## The next state to run as the FSM iterates
    state = 0
    
    ## The number of iterations of the FSM
    runs = 0
    ButtonFlag = False
    pinA5 = pyb.Pin (pyb.Pin.cpu.A5, pyb.Pin.OUT_PP)
    tim2 = pyb.Timer(2, freq = 20000)
    t2ch1 = tim2.channel(1, pyb.Timer.PWM, pin=pinA5)
    pinC13 = pyb.Pin (pyb.Pin.cpu.C13)
    ButtonInt = pyb.ExtInt(pinC13, mode = pyb.ExtInt.IRQ_FALLING, pull = pyb.Pin.PULL_NONE, callback = onButtonPressFCN)
    current_time = utime.ticks_ms()
    check = 0
    while(True):
        ButtonFlag = False
        try:
            
            if (state == 0):
                # run state 0
                check += 1
                if check == 1:
                    print('Press button B1 to cycle through the Square, Sine, and Sawtooth LED cycles')
                
                state = 1
#                print('In state 1')
            
            elif (state == 1):
                # run state 1
                
                if ButtonFlag == True:
                    state = 2
                    start2 = utime.ticks_ms()
#                    print('In state 2')
                    
            elif (state == 2):
                #run square wave
                
                current2 = utime.ticks_ms()
                current_time2 = utime.ticks_diff(current2, start2)
#                print(current_time2)
                pwm_percent = update_sqw(current_time2)
                t2ch1.pulse_width_percent(pwm_percent)
                if ButtonFlag == True:
                    state = 3
                    start3 = utime.ticks_ms()
#                    print('In state 3')
                
            elif (state == 3):
                #run sine wave
                current3 = utime.ticks_ms()
                current_time3 = utime.ticks_diff(current3, start3)
                pwm_percent = update_sin(current_time3)
                print(pwm_percent)
                t2ch1.pulse_width_percent(pwm_percent)
                if ButtonFlag == True:
                    state = 4
                    start4 = utime.ticks_ms()
#                    print('In state 4')

            elif (state == 4):
                #saw tooth
                current4 = utime.ticks_ms()
                current_time4 = utime.ticks_diff(current4, start4)
#                print(current_time4)
                pwm_percent = update_stw(current_time4)
#                print(pwm_percent)
                t2ch1.pulse_width_percent(pwm_percent)
                if ButtonFlag == True:
                    state = 2
                    start2 = utime.ticks_ms()
                        
                
            
        except KeyboardInterrupt:
            break
    print('Program Terminated')
    print('Number of runs: ', runs)


    
    