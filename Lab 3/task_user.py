# -*- coding: utf-8 -*-
"""
@file task_user.py
@author Jeremy Baechler
@author James Verheyden
@date 2021.10.20
@brief task_user.py is the file which handles all the user interfacing necessary on
labs 3 and 4.
"""

import pyb, utime
import array




class Task_User:
    ''' @brief Sets up the Task_User class
        @details Organizes all of the class methods for the task user UI. Task_User 
        is referenced in main.py and drives the bulk of lab 3. 
    '''
    
    def __init__(self, period, enc1, task1, enc2, task2, motor_drv, motor_1, motor_2, z_flag, p_flag, d_flag, g_flag, s_flag):
        '''
        @brief Initiates flags, timing, data caches, state transitions, and motor control.
        @details Initiates flags which convey when keys are pressed to perform tasks. 
        Sets a counter to zero and sets up 'next_time' which based off the period 
        determines how often to collect data. Creates the cache used by state 4 
        to collect and print encoder data. Sets the state equal to zero at the 
        end of each state iteration (with the exception for state 4).
        
        @param period
        period for Task_User
        @param enc1
        Encoder object 2
        @param task1
        Task_Encoder object 1
        @param enc2
        Encoder object 2
        @param task2
        Task_Encoder object 2
        @param motor_drv
        DRV8847 class object, motor driver
        @param motor_1
        Motor class object for motor 1
        @param motor_2
        Motor class obbject for motor 2
        
        '''
        ## @brief z flag which is toggled to True when the z character is pressed.
        self.z_flag = z_flag
        ## @brief p flag which is toggled to True when the p character is pressed.
        self.p_flag = p_flag
        ## @brief d flag which is toggled to True when the d character is pressed.
        self.d_flag = d_flag
        ## @brief g flag which is toggled to True when the g character is pressed.
        self.g_flag = g_flag
        ## @brief s flag which is toggled to True when the s character is pressed.
        self.s_flag = s_flag
        ## @brief stored class object defined as the period at which our program will run.
        self.period = period
        ## @brief class object for encoder 1.
        self.enc1 = enc1
        ## @brief class object for encoder task 1.
        self.task1 = task1
        ## @brief class object for encoder 2.
        self.enc2 = enc2
        ## @brief class object for encoder task 2.
        self.task2 = task2
        ## @brief class object for DRV8847 motor driver.
        self.motor_drv = motor_drv
        ## @brief class object for motor 1.
        self.motor_1 = motor_1
        ## @brief class object for motor 2.
        self.motor_2 = motor_2
        ## @brief counter object used indexing.
        self.count = 0
        ## @brief variable used to reference whether or not the input character is lowercase.
        self.lower = True
#        self.current_time = utime.ticks_us()
        ## @brief variable defined as the next time the task will run.
        self.next_time = utime.ticks_add(utime.ticks_us(), self.period)
        ## @brief An array object used to store position data from the encoder.
        self.pos_cache = array.array('f', [0]*3000)
        ## @brief An array object used to store time data during data collection.
        self.tim_cache = array.array('f', [0]*3000)
        ## @brief An array object which stores delta values from the encoder.
        self.delta_cache = array.array('f', [0]*3000)
        ## @brief integer variable used for iteration.
        self.i = 0
        ## @brief object which determines the state of the task.
        self.STATE = -1
        ## @brief variable used for indexing.
        self.idx = 0
        ## @brief serial port reference used to print and return values from the serial connection.
        self.serport = pyb.USB_VCP()
        ## @brief variable used to keep track of how much time has elapsed.
        self.time = 0
        ## @brief String used to store user input and share this data with various task states.
        self.num_data = ''
        ## @brief variable which is passed into the motor driver to alter motor 1's duty cycle.
        self.duty1 = 0
        ## @brief variable which is passed into the motor driver to alter motor 2's duty cycle.
        self.duty2 = 0

    def run(self):
        '''
        @brief A function that alternates between various task states based on user input.
        @details States (-1,0,1,2,3,4,5,6,7) are used to cycle between dirrent tasks. 
        -1 prints the help screen. 0 returns to start. 1 zeros the position encoder. 
        2 prints the position of the encoder. 3 prints the change in position 
        bewtwwen the current position and the zeroed position. 4 collects position 
        data for 30 seconds and returns a comma seperated array. State 5 is used to 
        print the position, time, and delta values of the chosen encoder. State 6 
        clears a fault condition if one is present. State 7 takes an input from 
        the user to set the desired PWM setpoint for a motor.
        
        @param self 
        The object pointer
        '''
        
        current_time = utime.ticks_us()
#        print(self.current_time, self.next_time)
#        print(utime.ticks_diff(current_time, self.next_time))
        
        if utime.ticks_diff(current_time, self.next_time) >= 0:
            
#            enc1 = encoder_updated.Encoder(pyb.Pin.cpu.B6, pyb.Pin.cpu.B7, 4)
#            task1 = task_encoder.Task_Encoder()
#            print(enc1.position)
            if self.STATE == 0:
#                print('In State 0')
                if self.serport.any():
#                    print('received an input')
                    val = self.serport.read(1).decode()
                    if val == 'z' or val == 'Z':
                        self.STATE = 1
                        if val == 'Z':
                            self.lower = False
                    elif val == 'p' or val == 'P':
                        self.STATE = 2
                        if val == 'P':
                            self.lower = False
                    elif val == 'd' or val == 'D':
                        self.STATE = 3
                        if val == 'D':
                            self.lower = False
                    elif val == 'g' or val == 'G':
                        self.STATE = 4
                        if val == 'G':
                            self.lower = False
                        print('Data collection has begun!')
                    elif val == 'c' or val == 'C':
                        self.STATE = 6
                    elif val == 'm' or val == 'M':
                        self.STATE = 7
                        if val == 'M':
                            self.lower = False
                    
                if self.STATE == 4 and (val == 's' or val == 'S'):
                    self.STATE = 5
                
                    
                    print('Data collection has ended')
                    
#            if self.g_flag == False:
#                
            if self.STATE == -1:
                print('Welcome to our Encoder Hardware UI!')
                print('''Please use these inputs to get data from the Nucleo: 
                    
z can be used to zero the position of encoder 1
Z can be used to zero the position of encoder 2
p can be used to print out the position of encoder 1
P can be used to print out the position of encoder 2
d can be used to print out the delta value of encoder 1
D can be used to print out the delta value of encoder 2
m can be used to set a duty cycle for motor 1
M can be used to set a duty cycle for motor 2
c or C can be used to clear a fault condition triggered by the DRV8847
g can be used to collect encoder 1 data for 30 seconds
G can be used to collect encoder 2 data for 30 seconds
s or S ends data collection prematurely during collection''')
                self.STATE = 0
        
                
                
            if self.STATE == 1:
#                print('We have pressed z')
                if self.lower == True:
                    self.enc1.update()
                    self.task1.z_flag = True
                    print('Encoder 1 has been zeroed! Encoder position: ', self.task1.run(self.enc1))
                    self.task1.z_flag = False
                elif self.lower == False:
                    self.enc2.update()
                    self.task2.z_flag = True
                    print('Encoder 2 has been zeroed! Encoder position: ', self.task2.run(self.enc2))
                    self.task2.z_flag = False
                self.STATE = 0
                self.lower = True
           
            if self.STATE == 2:
#                print('We have pressed p')
                if self.lower == True:
                    self.enc1.update()
                    self.task1.p_flag = True
                    print('Encoder 1 Position: ', self.task1.run(self.enc1))
                    self.task1.p_flag = False
                elif self.lower == False:
                    self.enc2.update()
                    self.task2.p_flag = True
                    print('Encoder 2 Position: ', self.task2.run(self.enc2))
                    self.task2.p_flag = False
                self.STATE = 0
                self.lower = True

            if self.STATE == 3:
#                print('We have pressed d')
                if self.lower == True:
                    self.enc1.update()
                    self.task1.d_flag = True
#                print(self.task1.z_flag, self.task1.p_flag, self.task1.d_flag, self.task1.g_flag, self.task1.s_flag)
                    print('Delta value from Encoder 1: ', self.task1.run(self.enc1))
                    self.task1.d_flag = False
#                print(task1.z_flag, task1.p_flag, task1.d_flag, task1.g_flag, task1.s_flag)
                elif self.lower == False:
                    self.enc2.update()
                    self.task2.d_flag = True
                    print('Delta value from Encoder 2: ', self.task2.run(self.enc2))
                    self.task2.d_flag = False
                self.STATE = 0
                self.lower = True
                    
            if self.STATE == 4:
                
#                current_time = utime.ticks_us()
#                print('We have pressed g')
                if self.lower == True:
                    self.enc1.update()
                    self.task1.g_flag = True
                    self.pos_cache[self.idx] = self.task1.run(self.enc1)*2*3.14159/4000
                    self.task1.g_flag = False
                    self.tim_cache[self.idx] = self.time
                    self.task1.d_flag = True
                    self.delta_cache[self.idx] = self.task1.run(self.enc1)*2*3.14159/4000/.01
                    self.task1.d_flag = False
                    self.time += .01
                    self.idx += 1
                    if self.time > 30:
                        print('Data collection has ended')
                        self.STATE = 5
                        
                        self.count = 0
#                        self.STATE = 0
                if self.serport.any():
#                        print('serport has a value')
                        if self.serport.read(1).decode() == 's' or self.serport.read(1).decode() == 'S':
                            self.STATE = 5
                        
                if self.lower == False:
                    self.enc2.update()
                    self.task2.g_flag = True
                    
#                    print(round(self.time, 2), self.task2.run(self.enc2))
                    self.pos_cache[self.idx] = self.task2.run(self.enc2)*2*3.14159/4000
                    self.task2.g_flag = False
                    self.tim_cache[self.idx] = self.time
                    self.task2.d_flag = True
                    self.delta_cache[self.idx] = self.task2.run(self.enc2)*2*3.14159/4000/.01
                    self.task2.d_flag = False
                    self.time += .01
                    self.idx += 1
                    if self.time > 30:
                        print('Data collection has ended')
                        self.STATE = 5
                        
                        self.count = 0
#                        self.STATE = 0
                    
                
            
            if self.STATE == 5:
                if self.i < len(self.pos_cache):
                        print('Position: %4.2f, Time: %4.2f, Delta: %4.2f' % (self.pos_cache[self.i], self.tim_cache[self.i], self.delta_cache[self.i]))
                        self.i += 1
                        self.lower = True
                if self.i > len(self.pos_cache):
                    self.STATE = 0
                    self.i = 0
                    self.tim_cache = array.array('f', [0]*3000)
                    self.pos_cache = array.array('f', [0]*3000)
                    self.delta_cache = array.array('f', [0]*3000)
            
            if self.STATE == 6:
                self.motor_drv.enable()
                self.STATE = 0
            
            
            if self.STATE == 7:
#                print('In State 7')
                if self.lower == True:
                    if self.serport.any():
                        val = self.serport.read(1)
                        if val.isdigit():
                            self.num_data += val.decode()
                            self.serport.write(val.decode())
                        elif val.decode() == '-' and len(self.num_data) == 0:
                            self.num_data += val.decode()
                            self.serport.write(val.decode())
                        elif val.decode() == '.':
                            self.num_data += val.decode()
                            self.serport.write(val.decode())
                        elif val.decode() == '\x7f' and len(self.num_data) > 0:
                            self.num_data = self.num_data[:-1]
                            self.serport.write(val.decode())
                        elif val.decode() == '\r' or val.decode() == '\n':
                            self.serport.write('\r\n')
                            self.duty1 = float(self.num_data)
#                            print('Duty Cycle is: ', self.duty1)
                            self.motor_1.set_duty(self.duty1)
                            self.STATE = 0
                            self.num_data = ''
                            
                elif self.lower == False:
                    if self.serport.any():
                        val = self.serport.read(1)
                        if val.isdigit():
                            self.num_data += val.decode()
                            self.serport.write(val.decode())
                        elif val.decode() == '-' and len(self.num_data) == 0:
                            self.num_data += val.decode()
                            self.serport.write(val.decode())
                        elif val.decode() == '.':
                            self.num_data += val.decode()
                            self.serport.write(val.decode())
                        elif val.decode() == '\x7f' and len(self.num_data) > 0:
                            self.num_data = self.num_data[:-1]
                            self.serport.write(val.decode())
                        elif val.decode() == '\r' or val.decode() == '\n':
                            self.duty2 = float(self.num_data)
#                            print('Duty Cycle is: ', self.duty2)
                            self.motor_2.set_duty(self.duty2)
                            self.STATE = 0
                            self.num_data = ''
                            self.lower = True
#                    else:
#                        print(val)
#                        self.serport.write(val.decode())
                        
                        
            
                        
                        
            self.next_time = utime.ticks_add(self.next_time, self.period)

