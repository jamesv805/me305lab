# -*- coding: utf-8 -*-
"""
Created on Thu Sep 23 15:25:19 2021
@brief Computes fibonacci numbers at a given index
@details Input the sequence number for the fibonacci number you want
            Negative numbers and non-intergers prompt a message
@param idx Desired index for fibonacci number
@return The fibonacci number at the desired index
@author: James Verheyden
"""
#This code solves for the Fibonacci Number
#The sequence number is inputed and the anwser is returned
#A message will be shown if a non-interger or a negative number is inputed


#CODE THAT SOLVES FOR THE FIBONACCI NUMBER
def fib (Q):        #Q is the number inputed
            N = 1
            Fa = 0
            Fb = 1
            while Q != N:       #This runs a loop and adds together
                N += 1          #all the fib numbers until the
                Fc = Fa + Fb    #specified one is reached
                Fa = Fb
                Fb = Fc
            return(Fb)
            
        
#CODE THAT DOES USSER INTERFACE
if __name__ == '__main__':
    while True:
        Q = input('Enter which number in Fibonacci sequence you want')
        try:                #This checks if the input is an interger
            Q=int(Q)
        except ValueError:
            print ('Must be an integer')
            continue
        #print (Q)
        if Q == 0:          #This gives an output if the input is 0
            print('The Fibonacci number is')
            print('0')
        elif Q == 1:        #This gives an output if the input is 1
            print('The Fibonacci number is')
            print('1')
        elif Q < 0 :        #This checks if the input is positive
            print('Must be positive')
        else:
            print('The Fibonacci number is')
            print(fib(Q))       #This gives the anwser
        end = input('Press e to exit, or press any key')    #exit comand
        if end == 'e':
            break
        else:
            continue
            pass