# -*- coding: utf-8 -*-
"""
@file DRV8847.py
"""

import pyb, utime



#nSLEEP.high()
#t3ch1.pulse_width_percent(100)
#t3ch2.pulse_width_percent(0)

class DRV8847:
    ''' 
    @brief A motor driver class for the DRV8847 object
    @details Objects of this class can be used to configure the DRV8847
    motor driver and to create one or more objects of the 
    Motor class which can be used to perform motor control.
    '''
    
    def __init__(self, nSLEEP):
        ''' 
        @brief
        '''
        self.pinB2 = pyb.Pin (pyb.Pin.cpu.B2)
        self.Fault = pyb.ExtInt(self.pinB2, mode = pyb.ExtInt.IRQ_FALLING, pull = pyb.Pin.PULL_NONE, callback = self.fault_cb)
        self.nSLEEP = nSLEEP
        
    
    def enable(self):
        '''
        @brief
        '''
        self.Fault.disable()
        self.nSLEEP.high()
        utime.sleep_us(25)
        self.Fault.enable()
        
    
    def disable(self):
        '''
        @brief
        '''
        self.nSLEEP.low()
        
    
    def fault_cb(self, IRQ_src):
        '''
        @brief
        @param
        '''
#        print('Fault has been triggered')
        self.nSLEEP.low()
        
    
    def motor(self, timchan1, timchan2):
        '''
        @brief
        '''
        return Motor(timchan1, timchan2)

class Motor:
    '''
    @brief      Initializes and return a motor object associated with the DRV8847.
    @details    Objects of this class should not be instantiated 
                directly. Instead create a DRV8847 object and use
                that to create Motor objects using the method
                DRV8847.motor().
    '''
    def __init__(self, timchan1, timchan2):
        '''
        @brief
        @details
        '''
        self.timchan1 = timchan1
        self.timchan2 = timchan2
        
    
    def set_duty(self, duty):
        '''
        @brief
        @details
        @param
        '''
        if duty > 100:
            duty = 100
        if duty < -100:
            duty = -100
        if duty >= 0 and duty <= 100:
            self.timchan1.pulse_width_percent(duty)
            self.timchan2.pulse_width_percent(0)
        elif duty < 0 and duty >= -100:
            self.timchan1.pulse_width_percent(0)
            self.timchan2.pulse_width_percent(abs(duty))
#        else:
#            print('Motor duty cycle must be between -100 and 100')
    
#if __name__ == '__main__':
    
    
    
    

    
    
    
    
#    print('Running Motor Duty')
#    
#    motor_1.set_duty(100)
#    motor_2.set_duty(60)
        
        
        
        
        