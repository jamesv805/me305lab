# -*- coding: utf-8 -*-
'''
@file closedloop.py
@brief B
@details C
@date 2021.10.7
@author Jeremy Baechler and James Verheyden

'''

class ClosedLoop:
    
    def __init__(self):
        self.Kp = 0
        
    
    def run(self, omega_ref, omega_meas):
        ''' @brief This method runs the closed loop calculations.
            @param omega_ref The reference velocity in rad/s defined by the user.
            @return This method returns the new duty cycle in PWM
        '''
        error = omega_ref-omega_meas
        duty = error * self.Kp
        return duty
        
    
    def get_Kp(self):
        ''' @brief Method designed to reference the self.Kp variable.
            @return Returns Kp value stored in the closed loop object.
        '''
        return self.Kp
    
    def set_Kp(self, Kp):
        ''' @brief Method that takes an input and changes the stored Kp value.
            @param Kp Desired proportional gain constant.

        '''
        self.Kp = Kp
        